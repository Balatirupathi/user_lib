package com.example.webservice2.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.webservice2.Entity.Users;

public interface UsersRepository extends JpaRepository<Users, Long> {

}
